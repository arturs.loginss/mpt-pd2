@extends('layouts.app')

@section('content')
    @include('report')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <h2>Turnīra rupjākie spēlētāji</h2>
                <p>Turnīra rupjāko spēlētāju (sakārtoti pēc piešķirto sodu skaita dilstošā secībā) saraksts.</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Vārds</th>
                        <th scope="col">Uzvārds</th>
                        <th scope="col">Komanda</th>
                        <th scope="col">Dzeltenās kartītes</th>
                        <th scope="col">Sarkanās kartītes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($rudeStats as $key => $rudeStat)
                        <tr>

                            <th scope="row">{{$key + 1}}</th>
                            <td>{{ $rudeStat['name'] }}</td>
                            <td>{{ $rudeStat['surname'] }}</td>
                            <td>{{ $rudeStat['team_name'] }}</td>
                            <td>{{ $rudeStat['yellow_card_count'] }}</td>
                            <td>{{ $rudeStat['red_card_count'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
