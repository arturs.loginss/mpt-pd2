@extends('layouts.app')

@section('content')
    @include('report')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <h2>Turnīra tabula</h2>
                <p>Turnīra tabulu (komandas vieta tabulā pēc kārtas, nosaukums,
                    iegūto punktu skaits, uzvaru un zaudējumu skaits pamatlaikā,
                    uzvaru un zaudējumu skaits papildlaikā, spēlēs gūto un zaudēto vārtu skaits).
                    Augstākā vietā jāatrodas komandai, kurai ir vairāk punktu.</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Komanda</th>
                        <th scope="col">Spēļu skaits</th>
                        <th scope="col">Punktu skaits</th>
                        <th scope="col">Uzvaras</th>
                        <th scope="col">Zaudējumi</th>
                        <th scope="col">Uzvaras papildlaikā</th>
                        <th scope="col">Zaudējumi papildlaikā</th>
                        <th scope="col">Gūtie vārti</th>
                        <th scope="col">Ielaistie vārti</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($teamStats as $key => $teamStat)
                        <tr>

                            <th scope="row">{{$key + 1}}</th>
                            <td>{{ $teamStat['name'] }}</td>
                            <td>{{ $teamStat['games_played_count'] }}</td>
                            <td>{{ $teamStat['points'] }}</td>
                            <td>{{ $teamStat['wins'] }}</td>
                            <td>{{ $teamStat['loses'] }}</td>
                            <td>{{ $teamStat['wins_ot'] }}</td>
                            <td>{{ $teamStat['loses_ot'] }}</td>
                            <td>{{ $teamStat['goals'] }}</td>
                            <td>{{ $teamStat['goals_against'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
