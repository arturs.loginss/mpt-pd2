@extends('layouts.app')

@section('content')
    @include('report')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <h2>Turnīra "stingrāko" tiesnešu saraksts</h2>
                <p>Turnīra "stingrāko" tiesnešu saraksts, kas sakārtots vidēji vienā spēlē piešķirto sodu skaita dilšanas secībā.</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Vārds</th>
                        <th scope="col">Uzvārds</th>
                        <th scope="col">Spēļu skaits, kā galvenais tiesnesis</th>
                        <th scope="col">Vidējais sodu skaits</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($referees as $key => $referee)
                        <tr>
                            <th scope="row">{{$key + 1}}</th>
                            <td>{{ $referee['name'] }}</td>
                            <td>{{ $referee['surname'] }}</td>
                            <td>{{ $referee['game_count_as_main'] }}</td>
                            <td>{{ $referee['videjais'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
