<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Pieejamās statistikas</h2>
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="{{ url('/') }}">{{ 'Turnīra Tabula' }}</a>
                </li>
                <li class="list-group-item">
                    <a href="{{ url('/player-stats') }}">{{ 'Turnīra desmit rezultatīvākie spēlētāji' }}</a>
                </li>
                <li class="list-group-item">
                    <a href="{{ url('/rude-stats') }}">{{ 'Turnīra rupjākie spēlētāji' }}</a>
                </li>
                <li class="list-group-item">
                    <a href="{{ url('/referees') }}">{{ 'Turnīra "stingrāko" tiesnešu saraksts' }}</a>
                </li>
            </ul>
            <hr>
        </div>
    </div>
</div>
