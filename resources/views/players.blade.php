@extends('layouts.app')

@section('content')
    @include('report')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 table-responsive">
                <h2>Turnīra desmit rezultatīvākie spēlētāji</h2>
                <p>Turnīra desmit rezultatīvāko spēlētāju
                    (sakārtoti pēc gūto vārtu skaita un rezultatīvo piespēļu skaita dilstošā secībā) saraksts.
                    Jānorāda vieta sarakstā pēc kārtas, spēlētāja vārds un uzvārds, komandas nosaukums,
                    gūto vārtu skaits un rezultatīvo piespēļu skaits. Sarakstā augstāk jāatrodas spēlētājam,
                    kas guvis vairāk vārtu, bet, vienāda gūto vārtu skaita gadījumā, tam spēlētājam,
                    kas vairāk reižu rezultatīvi piespēlējis.</p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Vārds</th>
                        <th scope="col">Uzvārds</th>
                        <th scope="col">Komanda</th>
                        <th scope="col">Vārtu skaits</th>
                        <th scope="col">Piespēļu skaits</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($playerStats as $key => $playerStat)
                        <tr>

                            <th scope="row">{{$key + 1}}</th>
                            <td>{{ $playerStat['name'] }}</td>
                            <td>{{ $playerStat['surname'] }}</td>
                            <td>{{ $playerStat['team_name'] }}</td>
                            <td>{{ $playerStat['goal_count'] }}</td>
                            <td>{{ $playerStat['assists'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
