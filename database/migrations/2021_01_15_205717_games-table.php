<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team1_id');
            $table->unsignedBigInteger('team2_id');
            $table->unsignedBigInteger('place_id');
            $table->date('date');
            $table->integer('viewers');
            $table->unsignedBigInteger('main_referee_id');
            $table->unsignedBigInteger('line_referee1_id');
            $table->unsignedBigInteger('line_referee2_id');
            $table->integer('team1_goals');
            $table->integer('team2_goals');
            $table->timestamps();
            $table->foreign('team1_id')->references('id')->on('teams');
            $table->foreign('team2_id')->references('id')->on('teams');
            $table->foreign('place_id')->references('id')->on('places');
            $table->foreign('main_referee_id')->references('id')->on('referees');
            $table->foreign('line_referee1_id')->references('id')->on('referees');
            $table->foreign('line_referee2_id')->references('id')->on('referees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
