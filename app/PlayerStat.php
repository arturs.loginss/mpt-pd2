<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerStat extends Model
{
    protected $fillable = ['player_id'];

    public function player()
    {
        return $this->belongsTo('App\Player');
    }
}
