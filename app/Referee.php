<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referee extends Model
{
    protected $fillable = ['name', 'surname', 'game_count_as_main', 'penalty_count'];
}
