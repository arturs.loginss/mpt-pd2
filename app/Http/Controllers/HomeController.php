<?php

namespace App\Http\Controllers;

use App\Http\Services\ReportService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var ReportService
     */
    private $reportService;

    /**
     * Create a new controller instance.
     *
     * @param ReportService $reportService
     */
    public function __construct(ReportService  $reportService)
    {
        $this->middleware('auth');

        $this->reportService = $reportService;
    }

    public function home()
    {
        return view('home');
    }
}
