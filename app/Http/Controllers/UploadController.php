<?php

namespace App\Http\Controllers;

use App\Http\Services\FileUploadService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * @var FileUploadService
     */
    private $fileUploadService;

    public function __construct(FileUploadService $fileUploadService)
    {
        $this->fileUploadService = $fileUploadService;
    }

    public function postUploadForm(Request $request)
    {
        try {
            $request->validate([
                'upload.*' => 'required|file|mimes:json|max:2048'
            ]);
            $result = "";
            if($request->hasFile('upload')) {
                foreach ($request->upload as $file) {
                    $content = file_get_contents($file);
                    $input = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($content));
                    $json = json_decode($input, true);

                    $result .= $file->getClientOriginalName() . "=" . $this->fileUploadService->processProtocolFile($json) . " | ";
                }
            }
            return back()
                ->with('status','Upload status: ' . $result)
                ->with('result', "ASD");
        } catch (\Exception $e) {
            $this->fileUploadService->storeUploadProgress(["file_name" => "unknown", "file_path" => "unknown", "status" => "error"]);
            return back()->withErrors(['Faila apstrādes kļūda: ' . $e->getMessage()]);
        }
    }
}
