<?php

namespace App\Http\Controllers;

use App\Http\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * @var ReportService
     */
    private $reportService;

    public function __construct(ReportService  $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $baseData = $this->reportService->getDataForBaseTable();
        return view('welcome')->with(["teamStats" => $baseData]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function players()
    {
        $baseData = $this->reportService->getTop10Players();
        return view('players')->with(["playerStats" => $baseData]);
    }

    public function rudePlayers()
    {
        $baseData = $this->reportService->getRudePlayers();
        return view('rude')->with(["rudeStats" => $baseData]);
    }

    public function referees()
    {
        $baseData = $this->reportService->getReferees();
        return view('referees')->with(["referees" => $baseData]);
    }

}
