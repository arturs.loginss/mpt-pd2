<?php


namespace App\Http\Services;

use App\PlayerStat;
use App\Referee;
use App\Team;
use App\TeamStat;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportService
 * @package App\Http\Services
 */
class ReportService
{
    public function __construct()
    {

    }

    public function getDataForBaseTable()
    {
        $teamStats = TeamStat::select('*')
            ->join('teams', 'team_stats.team_id', '=', 'teams.id')
            ->orderBy('team_stats.points', 'DESC')
            ->orderBy('team_stats.wins', 'DESC')
            ->get()->toArray();

        return $teamStats;
    }

    public function getTop10Players()
    {
        $playerStats = PlayerStat::join('players', 'player_stats.player_id', '=', 'players.id')
            ->join('teams', 'players.team_id', '=', 'teams.id')
            ->orderBy('player_stats.goal_count', 'DESC')
            ->orderBy('player_stats.assists', 'DESC')
            ->limit(10)
            ->get(['players.*','player_stats.*', 'teams.name as team_name'])->toArray();

        return $playerStats;
    }

    public function getRudePlayers()
    {
        $rudeStats = PlayerStat::where('yellow_card_count', '!=', 0)->orWhere('red_card_count', '!=', 0)
            ->join('players', 'player_stats.player_id', '=', 'players.id')
            ->join('teams', 'players.team_id', '=', 'teams.id')
            ->orderBy('player_stats.yellow_card_count', 'DESC')
            ->orderBy('player_stats.red_card_count', 'DESC')
            ->get(['players.*','player_stats.*', 'teams.name as team_name'])->toArray();

        return $rudeStats;
    }

    public function getReferees()
    {
        $referees = DB::select( DB::raw("SELECT name, surname, game_count_as_main, ROUND(penalty_count / game_count_as_main, 2) AS videjais  FROM referees WHERE game_count_as_main > 0 ORDER BY videjais desc"));

        return json_decode(json_encode($referees), true);

    }


}
