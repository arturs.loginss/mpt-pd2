<?php


namespace App\Http\Services;


use App\Game;
use App\Place;
use App\Player;
use App\PlayerStat;
use App\ProtocolUpload;
use App\Referee;
use App\Team;
use App\TeamStat;

/**
 * Class FileUploadService
 * @package App\Http\Services
 */
class FileUploadService
{
    private $overtimeGame = false;

    public function __construct()
    {

    }

    /**
     * @param array $data
     * @return string
     */
    public function processProtocolFile(array $data)
    {
        foreach ($data as $key => $value) {
            $placeId = $this->storeGamePlace($value["Vieta"]);
            $team1Id = $this->storeTeam($value["Komanda"][0]);
            $team2Id = $this->storeTeam($value["Komanda"][1]);
            $game = Game::where('date',$value["Laiks"])->whereIn('team1_id', [$team1Id, $team2Id])->first();
            if ($game) {
                return "Jau Eksistē!";
            }
            $team1Penalty = 0;
            $team2Penalty = 0;
            // Papildus pārbaude, jo Sodu objekts var būt tukšs, kas ir string vertība, vai arī tas var nebūt vispār.
            if(isset($value["Komanda"][0]["Sodi"]) && is_array($value["Komanda"][0]["Sodi"]) && !empty($value["Komanda"][0]["Sodi"])) {
                $team1Penalty = count($value["Komanda"][0]["Sodi"]);
            }

            if(isset($value["Komanda"][1]["Sodi"]) && is_array($value["Komanda"][1]["Sodi"]) && !empty($value["Komanda"][1]["Sodi"])) {
                $team2Penalty = count($value["Komanda"][0]["Sodi"]);
            }

            $mainRefereeId =  $this->storeReferee([
                "name" => $value["VT"]["Vards"],
                "surname" => $value["VT"]["Uzvards"],
                "penalty_count" => $team1Penalty + $team2Penalty,
                "game_count_as_main" => 1
            ]);

            $lineReferee1Id =  $this->storeReferee([
                "name" => $value["T"][0]["Vards"],
                "surname" => $value["T"][0]["Uzvards"],
            ]);
            $lineReferee2Id =  $this->storeReferee([
                "name" => $value["T"][1]["Vards"],
                "surname" => $value["T"][1]["Uzvards"],
            ]);

            $this->storeGame($placeId, $team1Id, $team2Id, $mainRefereeId, $lineReferee1Id, $lineReferee2Id, $value);
        }

        return "Pievienots";
    }

    /**
     * @param string $placeName
     * @return int
     */
    public function storeGamePlace(string $placeName): int
    {
        //Store place if not exist
        $place = Place::firstOrCreate(["name" => $placeName]);

        return $place->id;
    }

    /**
     * Store Team record in db
     * Store Player and all about player stats
     * @param array $team
     * @return int
     */
    public function storeTeam(array $team): int
    {
        //Store place if not exist
        $teamObject = Team::firstOrCreate(["name" => $team['Nosaukums']]);
        $teamStatsObject = TeamStat::firstOrCreate(["team_id" => $teamObject->id]);

        foreach ($team["Speletaji"]['Speletajs'] as $key => $value) {
            $player = $this->storePlayer([
                "name" => $value["Vards"],
                "surname" => $value["Uzvards"],
                "number" => $value["Nr"],
                "role" => $value["Loma"],
                "team_id" => $teamObject->id
            ]);

            PlayerStat::firstOrCreate(["player_id" => $player->id]);
        }

        $this->storePlayerStats($team, $teamObject->id);

        return $teamObject->id;
    }

    /**
     * @param array $referees
     * @return int
     */
    public function storeReferee(array $referees):int
    {
        $referee = Referee::where([
            ['name', '=', $referees['name']],
            ['surname', '=', $referees['surname']]
        ])->first();

        if(!$referee) {
            $referee = new Referee();
            $referee->name = $referees['name'];
            $referee->surname = $referees['surname'];
            $referee->penalty_count = $referees['penalty_count'] ?? 0;
            $referee->game_count_as_main = $referees['game_count_as_main'] ?? 0;

            $referee->save();
        } else {
            if (isset($referees['penalty_count'])) {
                $referee->penalty_count += $referees['penalty_count'];
            }

            if (isset($referees['game_count_as_main'])) {
                $referee->game_count_as_main += $referees['game_count_as_main'];
            }

            $referee->save();
        }

        return $referee->id;
    }

    /**
     * Store players data
     * @param array $playerData
     * @return Player
     */
    public function storePlayer(array $playerData): Player
    {
        return Player::firstOrCreate($playerData);
    }

    /**
     * Get players stats
     * @param $number
     * @param $team_id
     * @return PlayerStat
     */
    public function getPlayerStats($number, $team_id): PlayerStat
    {
        $player = Player::with('playerStat')
            ->where('number', $number)
            ->where('team_id', $team_id)->first();
        return $player->playerStat;
    }

    /**
     * Store players stats
     * @param $stats
     * @param $team_id
     */
    public function storePlayerStats($stats, $team_id)
    {
        foreach ($stats as $key => $stat) {
            switch ($key) {
                case "Pamatsastavs":
                    // Parbauda vai konkrētais spēlētājs ir pamatsastāvā
                    foreach ($stat['Speletajs'] as $gameStarted) {
                        $playerStats = $this->getPlayerStats($gameStarted, $team_id);
                        $playerStats->games_started += 1;
                        $playerStats->games_played += 1;
                        $playerStats->save();
                    }

                    break;
                case "Varti":
                    if (isset($stats["Varti"]) && is_array($stats["Varti"]) && !empty($stats["Varti"])) {
                        if (!$this->contains_array($stats["Varti"]['VG'])) {
                            $playerStats = $this->getPlayerStats($stats["Varti"]['VG']["Nr"], $team_id);
                            $playerStats->goal_count += 1;
                            $playerStats->save();

                            if ((int)str_replace(":", "", $stats["Varti"]['VG']["Laiks"]) > 6000) {
                                $this->overtimeGame = true;
                            }
                            break;
                        }

                        foreach ($stat['VG'] as $vg) {
                            $playerStats = $this->getPlayerStats($vg["Nr"], $team_id);
                            $playerStats->goal_count += 1;
                            $playerStats->save();
                            if ((int)str_replace(":", "", $vg["Laiks"]) > 6000) {
                                $this->overtimeGame = true;
                            }
                            if (isset($vg["P"])) {
                                foreach ($vg["P"] as $p) {
                                    if (isset($p["Nr"])) {
                                        $playerStats = $this->getPlayerStats($p["Nr"], $team_id);
                                        $playerStats->assists += 1;
                                        $playerStats->save();
                                    } else if ($p) {
                                        $playerStats = $this->getPlayerStats($p, $team_id);
                                        $playerStats->assists += 1;
                                        $playerStats->save();
                                    }
                                }
                            }
                        }
                    }

                    break;
                case "Mainas":
                    if (isset($stats["Mainas"]) && is_array($stats["Mainas"]) && !empty($stats["Mainas"])) {
                        if (!$this->contains_array($stats["Mainas"]['Maina'])) {
                            $playerStats = $this->getPlayerStats($stats["Mainas"]['Maina']["Nr2"], $team_id);
                            $playerStats->games_played += 1;
                            $playerStats->save();

                            break;
                        }

                        foreach ($stats["Mainas"]['Maina'] as $maina) {
                            $playerStats = $this->getPlayerStats($maina["Nr2"], $team_id);
                            $playerStats->games_played += 1;
                            $playerStats->save();
                        }
                    }

                    break;
                case "Sodi":
                    if (isset($stats["Sodi"]) && is_array($stats["Sodi"]) && !empty($stats["Sodi"])) {
                        $penalty = [];
                        if (!$this->contains_array($stats["Sodi"]['Sods'])) {
                            $playerStats = $this->getPlayerStats($stats["Sodi"]['Sods']["Nr"], $team_id);
                            $playerStats->yellow_card_count += 1;
                            $playerStats->save();

                            break;
                        }

                        foreach ($stats["Sodi"]['Sods'] as $sods) {
                            $playerStats = $this->getPlayerStats($sods["Nr"], $team_id);
                            if(in_array($sods["Nr"], $penalty)) {
                                $playerStats->red_card_count += 1;
                            } else {
                                array_push($penalty, $sods["Nr"]);
                                $playerStats->yellow_card_count += 1;
                            }
                            $playerStats->save();
                        }
                    }

                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param $team1Id
     * @param $team2Id
     * @param $team1Goals
     * @param $team2Goals
     */
    public function storeTeamStats($team1Id, $team2Id, $team1Goals, $team2Goals)
    {
        $team1Stat = TeamStat::where([
        ['team_id', '=', $team1Id]
        ])->first();

        $team2Stat = TeamStat::where([
            ['team_id', '=', $team2Id]
        ])->first();

        if ($team1Goals > $team2Goals && $this->overtimeGame == false) {
            $team1Stat->wins += 1;
            $team1Stat->goals += $team1Goals;
            $team1Stat->goals_against += $team2Goals;
            $team1Stat->points += 5;

            $team2Stat->loses += 1;
            $team2Stat->goals += $team2Goals;
            $team2Stat->goals_against += $team1Goals;
            $team2Stat->points += 1;
        } else if ($team1Goals < $team2Goals && $this->overtimeGame == false) {
            $team2Stat->wins += 1;
            $team2Stat->goals += $team2Goals;
            $team2Stat->goals_against += $team1Goals;
            $team2Stat->points += 5;

            $team1Stat->loses += 1;
            $team1Stat->goals += $team1Goals;
            $team1Stat->goals_against += $team2Goals;
            $team1Stat->points += 1;
        } else if ($team1Goals > $team2Goals && $this->overtimeGame == true) {
            $team1Stat->wins_ot += 1;
            $team1Stat->goals += $team1Goals;
            $team1Stat->goals_against += $team2Goals;
            $team1Stat->points += 3;

            $team2Stat->loses_ot += 1;
            $team2Stat->goals += $team2Goals;
            $team2Stat->goals_against += $team1Goals;
            $team2Stat->points += 2;
        } else {
            $team2Stat->wins_ot += 1;
            $team2Stat->goals += $team2Goals;
            $team2Stat->goals_against += $team1Goals;
            $team2Stat->points += 3;

            $team1Stat->loses_ot += 1;
            $team1Stat->goals += $team1Goals;
            $team1Stat->goals_against += $team2Goals;
            $team1Stat->points += 2;
        }

        $team1Stat->games_played_count += 1;
        $team2Stat->games_played_count += 1;


        $team1Stat->save();
        $team2Stat->save();
    }

    /**
     * Check if array contains array
     * @param $array
     * @return bool
     */
    public function contains_array($array){
        foreach($array as $value){
            if(is_array($value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $placeId
     * @param $team1Id
     * @param $team2Id
     * @param $mainRefereeId
     * @param $lineReferee1Id
     * @param $lineReferee2Id
     * @param $value
     */
    public function storeGame($placeId, $team1Id, $team2Id, $mainRefereeId, $lineReferee1Id, $lineReferee2Id, $value)
    {
        $game = new Game();
        $game->team1_id = $team1Id;
        $game->team2_id = $team2Id;
        $game->place_id = $placeId;
        $game->date = $value["Laiks"];
        $game->viewers = $value["Skatitaji"];
        $game->main_referee_id = $mainRefereeId;
        $game->line_referee1_id = $lineReferee1Id;
        $game->line_referee2_id = $lineReferee2Id;
        $team1Goals = 0;
        $team2Goals = 0;
        if(isset($value["Komanda"][0]["Varti"]["VG"]) && is_array($value["Komanda"][0]["Varti"]["VG"]) && !empty($value["Komanda"][0]["Varti"]["VG"])) {
            if (!$this->contains_array($value["Komanda"][0]["Varti"]["VG"])) {
                $team1Goals = 1;
            } else {
                $team1Goals = count($value["Komanda"][0]["Varti"]["VG"]);
            }

        }

        if(isset($value["Komanda"][1]["Varti"]["VG"]) && is_array($value["Komanda"][1]["Varti"]["VG"]) && !empty($value["Komanda"][1]["Varti"]["VG"])) {
            if (!$this->contains_array($value["Komanda"][1]["Varti"]["VG"])) {
                $team2Goals = 1;
            } else {
                $team2Goals = count($value["Komanda"][1]["Varti"]["VG"]);
            }
        }

        $game->team1_goals = $team1Goals;
        $game->team2_goals = $team2Goals;

        $game->save();

        $this->storeTeamStats($team1Id, $team2Id, $team1Goals, $team2Goals);

        $this->overtimeGame=false;
    }

    /**
     * Store
     * @param $metadata
     */
    public function storeUploadProgress($metadata)
    {
        $protocolUpload = new ProtocolUpload();
        $protocolUpload->file_name = $metadata['file_name'];
        $protocolUpload->file_path = $metadata['file_path'];
        $protocolUpload->status = $metadata['status'];
        $protocolUpload->save();
    }
}
