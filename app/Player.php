<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = ['name', 'surname', 'number', 'role', 'team_id'];

    public function playerStat()
    {
        return $this->hasOne('App\PlayerStat');
    }
}
