### Projekta uzstādīšana

1. Projekta datubāze tiek darbināta docker konteinerī, tāpēc uz datora ir jābūt uztādītam docker un docker-compose 
1.1. Kad tas ir izdarīts - startējam konteineri ar komandu - `docker-compose up -d`
2. Projekts izmanto PHP 7.4 un Laravel 7 - uz datora jābūt uzstādītam PHP 7.4 | Composer
3. Projekta biblotēku uzstādīšana izpildot komandu - `composer install`
4. Projekta frintend tiek uzstādīts izpildot sekojošas komandas - `npm install && nrpm run development`, jaņem vērā, ka uz datora ir jābūt uzstādītai NodeJS >= 12 
5. Projekta iestartēšana izpildot komandu - `php artisan serv `
6. Rediģējam .env failu un norādām DB pieslēgšanās parametrus 
7. Izpildām pirmreizējās migrācijas izpildot komandu - `php artisan migrate`
8. Saskarē veicam reģistrāciju un izveidojam sev kontu, lai varētu ugšuplādēt protokolus.  
