<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'ReportController@index')->name('home');
Route::get('/player-stats', 'ReportController@players')->name('players');
Route::get('/rude-stats', 'ReportController@rudePlayers')->name('rudePlayers');
Route::get('/referees', 'ReportController@referees')->name('referees');
Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::post('/upload-protocols', 'UploadController@postUploadForm');
